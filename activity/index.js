//1. Create a readingListActD folder. Inside create an index.html and index.js files for the first part of the activity and create a crud.js file for the second part of the activity.
//2. Once done with your solution, create a repo named 'readingListActD' and push your documents.
//3. Save the repo link on S32-C1

//Part 1:
/*
Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
1.)
Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section)
*/
// Code Here:
  let studGrade = prompt("Enter student grade:")
  let section;
  function isSection(){
    if (studGrade >= 81 && studGrade <= 120) {
      section = "Section Opal";
      console.log("Your score is " + studGrade + " You will become proceed to Grade 10 " + section);
    } else if (studGrade >= 121 && studGrade <= 160) {
      section = "Section Sapphire";
      console.log("Your score is " + studGrade + " You will become proceed to Grade 10 " + section);
    } else if (studGrade >= 161 && studGrade <= 200) {
      section = "Section Diamond";
      console.log("Your score is " + studGrade + " You will become proceed to Grade 10 " + section);
    } else {
      section = "Section Ruby";
      console.log("Your score is " + studGrade + " You will become proceed to Grade 10 " + section);
    }
  }

  isSection();

/*
2.) 
Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'
*/
// Code Here:
  function isTheLongest(str){
    let char = str.match(/\w[a-z]{0,}/gi);
    let result = char[0];

    for (x=1; x < char.length; x++) {
      if (result.length < char[x].length) {
        result = char[x]
      }
    }
    return result;
  }
  console.log(isTheLongest("Web Development Tutorial"))

/*
3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'
*/
// Code Here:
  function isRepeated(str) {
    let char = str.split('');
    let result = '';
    let ctr = 0;

  for (x=0; x < char.length; x++) {
    ctr = 0;

    for (y = 0; y < char.length; y++) 
    {
      if (char[x] === char[y]) {
        ctr+= 1;
      }
    }

    if (ctr < 2) {
      result = char[x];
      break;
    }
  }
  return result;
  }
  console.log(isRepeated('abacddbec'));

//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots
*/
// Code Here:
let products = [
  {
    "product_name": "Yoyo",
    "description": "Toy",
    "price": 150,
    "stocks": 150
  },
  {
    "product_name": "Sipa",
    "description": "Toy",
    "price": 100,
    "stocks": 100
  },
  {
    "product_name": "Stickers",
    "description": "For Design",
    "price": 200,
    "stocks": 200
  }
];

let http = require('http');
let port = 8000;
let server = http.createServer(function(request, response) {
    if (request.url == '/' && request.method == 'GET') {
      response.writeHead(200, {'Content-Type': 'application/json'})
      response.end("Welcome to Ordering System")

    } else if (request.url == '/dashboard' && request.method == 'GET') {
      response.writeHead(200, {'Content-Type': 'application/json'})
      response.end("Welcome to your User's Dashboard!")

    } else if (request.url == '/products' && request.method == 'GET') {
      response.writeHead(200, {'Content-Type': 'application/json'})
      response.write(JSON.stringify(products))
      response.end("Here's our products availble")

    } else if (request.url == '/addProduct' && request.method == 'POST') {
      let request_body = ' '

        request.on('data', function(data){
          request_body += data
          console.log(request_body)
        })
          request.on('end', function() {
          console.log(typeof request_body)

          request_body = JSON.parse(request_body)

          let new_product = {
            "product_name": request_body.product_name,
            "description": request_body.description,
            "price": request_body.price,
            "stocks": request_body.stocks
          }
          products.push(new_product)

          response.writeHead(200, {'Content-Type': 'application/json'})
          response.write(JSON.stringify(new_product))
          response.end('Added new products')
        })      

    } else if (request.url == '/products' && request.method == 'PUT') {
      response.writeHead(200, {'Content-Type': 'application/json'})
      response.end("Updated a product to our resources")

    } else if (request.url == '/products' && request.method == 'DELETE') {
      response.writeHead(200, {'Content-Type': 'application/json'})
      response.end("Archived product to our resources")

    } else {
      response.writeHead(404, {'Content-Type': 'text/plain'})
      response.end('Error 404. Page not availble')
    }
})

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`)